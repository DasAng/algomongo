package algomongo

import (
	"gopkg.in/mgo.v2"
	"log"
	"fmt"
	"time"
)

type MongodDbOption struct {
	Address string
}

type MongoDBConnection struct {
	Session                *mgo.Session
	Option                 *MongodDbOption
	OnStartupDatabaseConnected chan bool
}

// NewMongoDbConnection instantiates a new database connection instance
// The parameter MongodDbOption is an option instance where you can specify DB address.
// This method will return a pointer to an instance of MongoDBConnection.
func NewMongoDbConnection(option *MongodDbOption) *MongoDBConnection {
	var obj *MongoDBConnection
	obj = &MongoDBConnection{
		Option: &MongodDbOption{Address:"localhost:27017"},
		Session: nil,
		OnStartupDatabaseConnected:make(chan bool),
	}

	if option != nil {
		*obj.Option = *option
	}

	return obj
}

func (connection *MongoDBConnection) Connect() error {

	url := fmt.Sprintf("mongodb://%s",connection.Option.Address)

	session, err := mgo.Dial(url)

	if err == nil {
		log.Println("[INFO] Connected to MongoDB")
		connection.Session = session
		connection.OnStartupDatabaseConnected <- true
	}

	return err
}

func (connection *MongoDBConnection) Reconnect(pollTimeInSeconds int) {

	log.Printf("[INFO] Retry connecting to MongoDB every %d seconds\r\n",pollTimeInSeconds)

	for {
		<-time.After(time.Duration(pollTimeInSeconds) * time.Second)
		err := connection.Connect()
		if err == nil {
			return
		}
	}
}

func (connection *MongoDBConnection) CreateIndex(index mgo.Index,databaseName string,collectionName string) {

	if connection.Session != nil {
		session := connection.Session.Copy()
		col := session.DB(databaseName).C(collectionName)
		col.EnsureIndex(index)
		defer session.Close()
	}
}

func (connection *MongoDBConnection) WaitForConnection() {
	<- connection.OnStartupDatabaseConnected
}

